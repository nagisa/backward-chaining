from string import ascii_uppercase
from itertools import tee, zip_longest
import sys

class Rule:
  def __init__(self, o, i):
    self.o = o
    self.i = i
    self.f1 = False

  def __repr__(self):
    return "{} → {}".format(", ".join(map(chr, self.i)), chr(self.o))

def error(cond, s):
  if cond:
    sys.stderr.write(s)
    sys.stderr.write("\n")
    sys.exit(1)

# Duomenų nuskaitymo funkcija
def read_rules(fname):
  it = (line.split('#')[0].strip() for line in open(fname, "r")
        if line and not line.startswith("#"))
  it, n = tee(it, 2)
  it = enumerate(zip_longest(it, n))
  (rules, facts) = ([], [])
  next(n)
  for lineno, (line, peek) in it:
    split = line.split()
    out = split[0]
    i = split[1:]
    error(not all(c in ascii_uppercase for c in i) or
          not out in ascii_uppercase,
          ("Negera eilutė `{}`! " +
           "Tikimasi produkcijos "+
           "(turi būti sudaryta iš didžiųjų ASCII raidžių)"
          ).format(line))
    rules.append(Rule(ord(out), list(map(ord, i))))
    if peek.startswith("+") or peek.startswith("="):
      break;
  for lineno, (line, peek) in it:
    error(not line.startswith("+"),
          ("Negera eilutė `{}`! Tikimasi GDB įrašų " +
           "(eilutės turi prasidėti +)"
          ).format(line))
    for f in line.split()[1:]:
      facts.append(ord(f))
    if peek.startswith("="):
      break;
  (lineno, (line, peek)) = next(it)
  error(not line.startswith("="),
        ("Negera eilutė `{}`! Tikimasi tikslo " +
         "(eilutė turi prasidėti =)"
        ).format(line))
  goals = list(map(ord, line.split()[1:]))
  error(len(goals) != 1,
        "Nurodyta daugiau nei 1 tikslas. Įveskite taisyklę.")
  return (rules, facts, goals[0])

# BEGIN
def apply_rules(rules, facts, goal, depth = 1, of=None, in_prog=None):
  prefix = "."*depth
  of = list(facts) if of is None else of
  def ff(x):
    rst = ", ".join(map(chr, filter(lambda y: y not in of, x)))
    return ("{} ir {}" if rst else "{}").format(*(", ".join(map(chr, of)), rst))
  if in_prog is not None and goal in in_prog:
    print("{}Tikslas {}. Ciklas. Grįžtame, FAIL."
          .format(prefix, chr(goal)))
    return None
  if goal in facts:
    fms = "{}Tikslas {}. ".format(prefix, chr(goal))+"Faktas ({}), nes faktai {}. Grįžtame, sėkmė."
    if goal in of:
      print(fms.format("duotas", ", ".join(map(chr, of))))
    else:
      print(fms.format("buvo gautas", ff(facts)))
    return ([], facts)
  # Ieškome tinkamos pritaikyti taisyklės
  for ruleno, rule in enumerate(rules):
    if rule.o != goal: continue
    ruleno += 1
    new_in_prog = set() if not in_prog else set(in_prog)
    new_in_prog.add(rule.o)
    print("{}Tikslas {}. Randame R{}:{}. Nauji tikslai {}."
          .format(prefix, chr(goal), ruleno, rule, ", ".join(map(chr, rule.i))))
    new_facts = list(facts)
    rs = []
    # Bandome išvesti visas kairės pusės taisykles
    for inp in rule.i:
      r = apply_rules(rules, new_facts, inp, depth=depth+1, of=of, in_prog=new_in_prog)
      if r is None:
        # Nepavyko išvesti kairės pusės taisyklės.
        break
      else:
        (r, new_facts) = r
        rs.extend(r)
    else:
      # Visos kairės pusės taisyklės išvestos.
      new_facts.append(goal)
      rs.append(ruleno)
      print(("{}Tikslas {}. Faktas (dabar gautas). Faktai {}. Grįžtame, sėkmė.")
            .format(prefix, chr(goal), ff(new_facts)))
      return (rs, new_facts)
  print("{}Tikslas {}. Nėra taisyklių jo išvedimui. Grįžtame, FAIL."
        .format(prefix, chr(goal)))
# END

rfg = read_rules(sys.argv[1])
print("1 DALIS. ĮVESTIS")
print("TAISYKLĖS")
for rn, rule in enumerate(rfg[0]):
    print("  R{}:".format(rn+1), str(rule))
print("FAKTAI: {}".format(", ".join(map(chr, rfg[1]))))
print("TIKSLAS: {}\n".format(chr(rfg[2])))
if rfg[2] in rfg[1]:
  print("3 DALIS. ATSAKYMAS:\n  TIKSLAS TARP FAKTŲ.")
  sys.exit(0)
else:
  print("2 DALIS. ALGORITMO ŽINGSNIAI")
  rs = apply_rules(*rfg)
if rs is not None:
  rs = rs[0]
  print("\n3 DALIS. ATSAKYMAS\n  TIKSLAS IŠVEDAMAS.\n  {}"
        .format(", ".join(("R{}".format(r) for r in rs))))
else:
  print("\n3 DALIS. ATSAKYMAS\n  TIKSLAS NEIŠVEDAMAS!")
