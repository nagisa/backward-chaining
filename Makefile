SHELL=/bin/bash
LATEX_FLAGS = -shell-escape
INS = test1 test2 test3 test4 test5 test5 test6 test7 test8 test9
OUTS = $(patsubst %,%.out,$(INS))
GRAPHS := $(shell echo test{1,2,3,4,5,6,7,9}{,-ver}.gv.tex) test5-expl.gv.tex
IMAGES = VU_L_CL.svg.pdf

TEX_DEPS = rgdb.py.cut lapas.tex $(IMAGES) $(INS) $(OUTS) $(GRAPHS) lapas.bbl

all: lapas.pdf

clean:
	rm -rf $(OUTS) $(GRAPHS) *.pdf lapas.bbl lapas.toc lapas.aux lapas.blg lapas.bcf lapas.out lapas.log lapas.run.xml lapas.auxlock _minted-lapas/

lapas.pdf: $(TEX_DEPS) lapas.toc
	lualatex $(LATEX_FLAGS) lapas

lapas.toc: $(TEX_DEPS)
	lualatex $(LATEX_FLAGS) lapas

PATH := ${PATH}:/usr/bin/vendor_perl/
lapas.bbl: lapas.bib lapas.aux lapas.bcf
	biber lapas

lapas.aux lapas.bcf: lapas.tex $(IMAGES)
	lualatex $(LATEX_FLAGS) lapas

rgdb.py.cut: rgdb.py
	sed '1,/BEGIN/d;/END/,$$d' $< > $@

%.svg.pdf: %.svg
	inkscape -A $@ $<

%.out: % rgdb.py
	python rgdb.py $< > $@

%.gv.tex: %.gv
	dot2tex --codeonly --nodeoptions=align=center -tmath -ftikz $< -o$@
